Open Weather Client Library

Designed as a single facade service to allocate the consumption of https://openweathermap.org multiple api(s).
Install

npm i ow-client-lib --save
Setup

step 1: add OwClientLibModule to app NgModule, make sure you have provided openweathermap.org api key as APP_ID value


import { OwClientLibModule } from 'ow-client-lib';

@NgModule({
  imports: [
    OwClientLibModule // OwClientLibModule added
  ],
  bootstrap: [App],
  declarations: [App],
  providers: [
    { provide: 'APP_ID', useValue: '<API Key>' }, // this is api key of openweathermap.org
  ],
})
class MainModule {}
Use


import { OwClientLibService } from 'ow-client-lib';

@Component({...})
export class YourComponent implements OnInit {
  constructor(private owClientLibService: OwClientLibService) {}

  public ngOnInit(): void {
    this._owClientLibService.GetFiveDaysForecastByCityName('London', 'GB').subscribe(response => console.log(response));
  }
}