import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';
import {FiveDayService} from './services/fivedays/fivedays.service';
import {SixteenDayService} from './services/sixteendays/sixteendays.service';
import {CurrentService} from './services/current/current.service';
import {IFiveDaysForecastGetResponse} from './contracts/five-days-forecast-get-response.interface';
import {ISixteenDaysForecastGetResponse} from './contracts/sixteen-days-forecast-get-response.interface';
import {IFiveDaysForecastShortResponse} from './contracts/five-days-forecast-short-response.interface';
import {ISixteenDaysForecastShortResponse} from './contracts/sixteen-days-forecast-short-response.interface';
import {ICurrentForecastGetResponse} from './contracts/current-forecast-get-response.interface';
import {OWRequest} from './core/ow-request.class';
import {OWRequestType} from './contracts/ow-request-type.enum';
import {UnitType} from './contracts/units.enum';
import {IOWRequestSetting} from './core/ow-request-setting.interface';

@Injectable({
  providedIn: 'root'
})
export class OwClientLibService {

  private _fiveDayService: FiveDayService;
  private get fiveDayService(): FiveDayService {
    if (!this._fiveDayService) {
      this._fiveDayService = this.injector.get(FiveDayService);
    }
    return this._fiveDayService;
  }

  private _sixteenDayService: SixteenDayService;
  private get sixteenDayService(): SixteenDayService {
    if (!this._sixteenDayService) {
      this._sixteenDayService = this.injector.get(SixteenDayService);
    }
    return this._sixteenDayService;
  }

  private _currentService: CurrentService;
  private get currentService(): CurrentService {
    if (!this._currentService) {
      this._currentService = this.injector.get(CurrentService);
    }
    return this._currentService;
  }

  private GetOWRequest(setting: IOWRequestSetting): OWRequest {
    const owReq = new OWRequest({
      queryName: setting.queryName,
      countryCode: setting.countryCode,
      type: setting.type,
      units: setting.units
    });
    return owReq;
  }

  constructor(private injector: Injector) {
  }

  public GetFiveDaysForecastByCityName(cityName: string,
    countryCode: string, units: UnitType): Observable<IFiveDaysForecastGetResponse> {
    const owReq = this.GetOWRequest({
      queryName: cityName,
      countryCode: countryCode,
      type: OWRequestType.Name,
      units: units
    });

    return this.fiveDayService.getWeatherData(owReq);
  }

  public GetSixteenDaysForecastByCityName(cityName: string,
    countryCode: string, units: UnitType): Observable<ISixteenDaysForecastGetResponse> {
    const owReq = this.GetOWRequest({
      queryName: cityName,
      countryCode: countryCode,
      type: OWRequestType.Name,
      units: units
    });

    return this.sixteenDayService.getWeatherData(owReq);
  }

  public GetFiveDaysForecastByZipCode(zipCode: string,
    countryCode: string, units: UnitType): Observable<IFiveDaysForecastGetResponse> {
    const owReq = this.GetOWRequest({
      queryName: zipCode,
      countryCode: countryCode,
      type: OWRequestType.ZipCode,
      units: units
    });

    return this.fiveDayService.getWeatherData(owReq);
  }

  public GetSixteenDaysForecastByZipCode(zipCode: string,
    countryCode: string, units: UnitType): Observable<ISixteenDaysForecastGetResponse> {
    const owReq = this.GetOWRequest({
      queryName: zipCode,
      countryCode: countryCode,
      type: OWRequestType.ZipCode,
      units: units
    });

    return this.sixteenDayService.getWeatherData(owReq);
  }

  public GetTodayForecastByCityName(cityName: string, countryCode: string, units: UnitType): Observable<ICurrentForecastGetResponse> {

    const owReq = this.GetOWRequest({
      queryName: cityName,
      countryCode: countryCode,
      type: OWRequestType.Name,
      units: units
    });

    return this.currentService.getWeatherData(owReq);
  }

  public GetTodayForecastByZipCode(zipCode: string, countryCode: string, units: UnitType): Observable<ICurrentForecastGetResponse> {

    const owReq = this.GetOWRequest({
      queryName: zipCode,
      countryCode: countryCode,
      type: OWRequestType.ZipCode,
      units: units
    });

    return this.currentService.getWeatherData(owReq);
  }

  public get fiveDayStream(): Observable<IFiveDaysForecastShortResponse> {
    return this.fiveDayService.owFiveDay$;
  }

  public get sixteenDayStream(): Observable<ISixteenDaysForecastShortResponse> {
    return this.sixteenDayService.owSixteenDay$;
  }

  public get currentStream(): Observable<ICurrentForecastGetResponse> {
    return this.currentService.owCurrentDay$;
  }
}
