import { Injectable, Inject, Optional } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { OW_API_URL } from '../environment';

@Injectable()
export class ServerHostInterceptor implements HttpInterceptor {

  constructor(@Inject('OW_API_URL') @Optional() private serverUrl: string,
  @Inject('APP_ID') @Optional() private appid: string) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const concatAppid = `&appid=${this.appid}`;
    req = req.clone({
        url: [this.serverUrl || OW_API_URL.serverUrl, req.url, concatAppid].join('')
      });
    return next.handle(req);
  }
}
