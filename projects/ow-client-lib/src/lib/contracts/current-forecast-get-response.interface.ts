import { ICoord } from '../core/coord.interface';
import { IWeather } from '../core/weather.interface';
import { IMain } from '../core/main.interface';
import { IWind } from '../core/wind.interface';
import { IClouds } from '../core/clouds.interface';

export interface ICurrentForecastGetResponse {
    coord: ICoord;
    sys: {country: string};
    weather: Array<IWeather>;
    base: string;
    main: IMain;
    wind: IWind;
    clouds: IClouds;
    dt: string;
    id: number;
    name: string;
}
