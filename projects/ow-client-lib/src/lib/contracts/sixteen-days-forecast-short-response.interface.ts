import { ISixteenDaysForecastData } from '../core/sixteen-days-forecasr-data.interface';
import { ICitySixTeenDaysForecast } from '../core/city-sixteen-days-forecast.interface';

export interface ISixteenDaysForecastShortResponse {
    city: ICitySixTeenDaysForecast;
    list: Array<ISixteenDaysForecastData>;
}
