import { ISixteenDaysForecastData } from '../core/sixteen-days-forecasr-data.interface';
import { ICitySixTeenDaysForecast } from '../core/city-sixteen-days-forecast.interface';

export interface ISixteenDaysForecastGetResponse {
    cod: number;
    message: number;
    city: ICitySixTeenDaysForecast;
    cnt: number;
    list: Array<ISixteenDaysForecastData>;
}
