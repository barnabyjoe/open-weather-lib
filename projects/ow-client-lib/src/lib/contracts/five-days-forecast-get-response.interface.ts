import { ICityFiveDaysForecast } from '../core/city-five-days-forecast.interface';
import { IFiveDaysForecastData } from '../core/five-days-forecast-data.interface';

export interface IFiveDaysForecastGetResponse {
    cod: number;
    message: number;
    city: ICityFiveDaysForecast;
    cnt: number;
    list: Array<IFiveDaysForecastData>;
}
