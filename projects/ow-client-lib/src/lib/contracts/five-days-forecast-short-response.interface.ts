import { ICityFiveDaysForecast } from '../core/city-five-days-forecast.interface';
import { IFiveDaysForecastData } from '../core/five-days-forecast-data.interface';

export interface IFiveDaysForecastShortResponse {
    city: ICityFiveDaysForecast;
    list: Array<IFiveDaysForecastData>;
}
