export const OW_API_URL = {
    serverUrl: '//api.openweathermap.org/data/2.5',
    FIVEDAYS: '/forecast',
    SIXTEENDAYS: '/forecast/daily',
    CURRENT: '/weather'
  };
