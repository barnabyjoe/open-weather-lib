import {TestBed, fakeAsync, tick} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import {ApiService} from '../api.service';
import {CurrentService} from './current.service';
import {ICurrentForecastGetResponse} from '../../contracts';
import {IOWRequestSetting} from '../../core/ow-request-setting.interface';
import {OWRequestType} from '../../contracts/ow-request-type.enum';
import {UnitType} from '../../contracts/units.enum';

describe('CurrentService', () => {
    let service: CurrentService;
    let api: ApiService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [CurrentService, ApiService]
        });
        api = TestBed.get(ApiService);
        service = new CurrentService(api);
        httpTestingController = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });


    it('should return an Observable<ICurrentForecastGetResponse>',
        fakeAsync(() => {
            const mockResponse: ICurrentForecastGetResponse = {
                base: '',
                clouds: null,
                coord: null,
                dt: '1535970501814',
                id: 1,
                main: null,
                name: '',
                sys: null,
                weather: null,
                wind: null
            };
            const setting: IOWRequestSetting = {
                queryName: 'London',
                countryCode: 'GB',
                type: OWRequestType.Name,
                units: UnitType.metric
            };

            service.getWeatherData(setting).subscribe((data: ICurrentForecastGetResponse) => {
                expect(data).toBeDefined();
                expect(data.main).toEqual(null);
                expect(data.dt).toEqual('1535970501814');
            });
            const req = httpTestingController.expectOne(x => x.url === '/weather?q=London,GB&units=metric');
            expect(req.request.method).toEqual('GET');

            req.flush(mockResponse);
            tick();
        })
    );
});
