import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';

import {ApiService} from '../api.service';
import {OW_API_URL} from '../../environment';
import {IDays} from '../base/days.interfaces';
import {ICurrentForecastGetResponse} from '../../contracts/current-forecast-get-response.interface';
import {IOWRequestSetting} from '../../core/ow-request-setting.interface';


@Injectable()
export class CurrentService implements IDays {

    private owCurrentSource: Subject<ICurrentForecastGetResponse> = new Subject<ICurrentForecastGetResponse>();

    public serviceUrl = OW_API_URL.CURRENT;

    public owCurrentDay$: Observable<ICurrentForecastGetResponse> = this.owCurrentSource.asObservable();

    constructor(private apiService: ApiService) {
    }

    public getWeatherData(setting: IOWRequestSetting): Observable<ICurrentForecastGetResponse> {
        return this.apiService.get(`${this.serviceUrl}?${setting.type}=${setting.queryName},${setting.countryCode}&units=${setting.units}`)
            .pipe(
                map((res: ICurrentForecastGetResponse) => {
                    this.changeDocument(res);
                    return res;
                })
            );
    }

    private changeDocument(resp: ICurrentForecastGetResponse): void {
        this.owCurrentSource.next(resp);
    }
}
