import {TestBed} from '@angular/core/testing';
import {HttpClient, HttpClientModule, HttpParams} from '@angular/common/http';

import {ApiService} from './api.service';

describe('ApiService', () => {
  let httpClient: HttpClient;
  let service: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [
        ApiService
      ]
    });

    httpClient = TestBed.get(HttpClient);
    service = new ApiService(httpClient);

    spyOn(httpClient, 'post').and.callFake(() => {
    });
    spyOn(httpClient, 'get').and.callFake(() => {
    });
    spyOn(httpClient, 'put').and.callFake(() => {
    });
    spyOn(httpClient, 'patch').and.callFake(() => {
    });
  });

  it('should be created', () => {
    // arrange

    // act

    // assert
    expect(service).toBeTruthy();
  });

  it('should call post()', () => {
    // arrange

    // act
    service.post('callsheet', {});

    // assert
    expect(httpClient.post).toHaveBeenCalled();
  });

  it('should call get()', () => {
    // arrange

    // act
    service.get('callsheet', new HttpParams());

    // assert
    expect(httpClient.get).toHaveBeenCalled();
  });

  it('should call put()', () => {
    // arrange

    // act
    service.put('callsheet', {});

    // assert
    expect(httpClient.put).toHaveBeenCalled();
  });

  it('should call patch()', () => {
    // arrange

    // act
    service.patch('callsheet');

    // assert
    expect(httpClient.patch).toHaveBeenCalled();
  });
});
