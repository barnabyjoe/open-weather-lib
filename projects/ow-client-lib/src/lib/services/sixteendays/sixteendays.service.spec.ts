import {TestBed, fakeAsync, tick} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import {ApiService} from '../api.service';
import {SixteenDayService} from './sixteendays.service';
import {ISixteenDaysForecastGetResponse} from '../../contracts';
import {IOWRequestSetting} from '../../core/ow-request-setting.interface';
import {OWRequestType} from '../../contracts/ow-request-type.enum';
import {UnitType} from '../../contracts/units.enum';

describe('CurrentService', () => {
    let service: SixteenDayService;
    let api: ApiService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [SixteenDayService, ApiService]
        });
        api = TestBed.get(ApiService);
        service = new SixteenDayService(api);
        httpTestingController = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });


    it('should return an Observable<ISixteenDaysForecastGetResponse>',
        fakeAsync(() => {
            const mockResponse: ISixteenDaysForecastGetResponse = {

                city: {name: 'London', country: 'GB', type: null, geoname_id: null, iso2: null, lat: null, lon: null, population: 10000},
                cnt: 7,
                cod: 200,
                list: null,
                message: null
            };
            const setting: IOWRequestSetting = {
                queryName: 'London',
                countryCode: 'GB',
                type: OWRequestType.Name,
                units: UnitType.metric
            };

            service.getWeatherData(setting).subscribe((data: ISixteenDaysForecastGetResponse) => {
                expect(data).toBeDefined();
                expect(data.city.country).toEqual('GB');
                expect(data.cod).toEqual(200);
            });
            const req = httpTestingController.expectOne(x => x.url === '/forecast/daily?q=London,GB&units=metric');
            expect(req.request.method).toEqual('GET');

            req.flush(mockResponse);
            tick();
        })
    );
});
