import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';

import {ApiService} from '../api.service';
import {OW_API_URL} from '../../environment';
import {IDays} from '../base/days.interfaces';
import {ISixteenDaysForecastGetResponse} from '../../contracts/sixteen-days-forecast-get-response.interface';
import {ISixteenDaysForecastShortResponse} from '../../contracts/sixteen-days-forecast-short-response.interface';
import {IOWRequestSetting} from '../../core/ow-request-setting.interface';


@Injectable()
export class SixteenDayService implements IDays {

    private owSixteenDaySource: Subject<ISixteenDaysForecastShortResponse> = new Subject<ISixteenDaysForecastShortResponse>();

    public serviceUrl = OW_API_URL.SIXTEENDAYS;

    public owSixteenDay$: Observable<ISixteenDaysForecastShortResponse> = this.owSixteenDaySource.asObservable();

    constructor(private apiService: ApiService) {
    }

    public getWeatherData(setting: IOWRequestSetting): Observable<ISixteenDaysForecastGetResponse> {
        return this.apiService.get(`${this.serviceUrl}?${setting.type}=${setting.queryName},${setting.countryCode}&units=${setting.units}`)
            .pipe(
                map((res: ISixteenDaysForecastGetResponse) => {
                    this.changeDocument(res);
                    return res;
                })
            );
    }

    private changeDocument(resp: ISixteenDaysForecastShortResponse): void {
        this.owSixteenDaySource.next(resp);
    }
}
