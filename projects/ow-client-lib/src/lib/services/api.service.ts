import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) {
  }

  post(path: string, model: any = null): Observable<any> {
    return this.http.post(`${path}`, model);
  }

  get(path: string, params: HttpParams = null): Observable<any> {
    return this.http.get(`${path}`, {params: params});
  }

  put(path: string, body: any): Observable<any> {
    return this.http.put(`${path}`, {body: body});
  }

  patch(path: string, model: any = null): Observable<any> {
    return this.http.patch(`${path}`, model);
  }
}
