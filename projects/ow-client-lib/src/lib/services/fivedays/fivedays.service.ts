import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';

import {ApiService} from '../api.service';
import {OW_API_URL} from '../../environment';
import {IDays} from '../base/days.interfaces';
import {IFiveDaysForecastGetResponse} from '../../contracts/five-days-forecast-get-response.interface';
import {IFiveDaysForecastShortResponse} from '../../contracts/five-days-forecast-short-response.interface';
import {IOWRequestSetting} from '../../core/ow-request-setting.interface';


@Injectable()
export class FiveDayService implements IDays {

    private owFiveDaySource: Subject<IFiveDaysForecastShortResponse> = new Subject<IFiveDaysForecastShortResponse>();

    public serviceUrl = OW_API_URL.FIVEDAYS;

    public owFiveDay$: Observable<IFiveDaysForecastShortResponse> = this.owFiveDaySource.asObservable();

    constructor(private apiService: ApiService) {
    }

    public getWeatherData(setting: IOWRequestSetting): Observable<IFiveDaysForecastGetResponse> {
        return this.apiService.get(`${this.serviceUrl}?${setting.type}=${setting.queryName},${setting.countryCode}&units=${setting.units}`)
            .pipe(
                map((res: IFiveDaysForecastGetResponse) => {
                    this.changeDocument(res);
                    return res;
                })
            );
    }

    private changeDocument(resp: IFiveDaysForecastShortResponse): void {
        this.owFiveDaySource.next(resp);
    }
}
