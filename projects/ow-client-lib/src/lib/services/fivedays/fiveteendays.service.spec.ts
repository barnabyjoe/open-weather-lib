import {TestBed, fakeAsync, tick} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import {ApiService} from '../api.service';
import {FiveDayService} from './fivedays.service';
import {IFiveDaysForecastGetResponse} from '../../contracts';
import {IOWRequestSetting} from '../../core/ow-request-setting.interface';
import {OWRequestType} from '../../contracts/ow-request-type.enum';
import {UnitType} from '../../contracts/units.enum';

describe('CurrentService', () => {
    let service: FiveDayService;
    let api: ApiService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [FiveDayService, ApiService]
        });
        api = TestBed.get(ApiService);
        service = new FiveDayService(api);
        httpTestingController = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });


    it('should return an Observable<IFiveDaysForecastGetResponse>',
        fakeAsync(() => {
            const mockResponse: IFiveDaysForecastGetResponse = {
                city: {name: 'London', coord: null, country: 'GB'},
                cnt: 7,
                cod: 200,
                list: null,
                message: null
            };
            const setting: IOWRequestSetting = {
                queryName: 'London',
                countryCode: 'GB',
                type: OWRequestType.Name,
                units: UnitType.metric
            };

            service.getWeatherData(setting).subscribe((data: IFiveDaysForecastGetResponse) => {
                expect(data).toBeDefined();
                expect(data.city.name).toEqual('London');
                expect(data.cod).toEqual(200);
            });
            const req = httpTestingController.expectOne(x => x.url === '/forecast?q=London,GB&units=metric');
            expect(req.request.method).toEqual('GET');

            req.flush(mockResponse);
            tick();
        })
    );
});
