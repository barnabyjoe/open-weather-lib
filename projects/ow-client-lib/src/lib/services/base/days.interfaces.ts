import {Observable} from 'rxjs';
import {IFiveDaysForecastGetResponse} from '../../contracts/five-days-forecast-get-response.interface';
import {ISixteenDaysForecastGetResponse} from '../../contracts/sixteen-days-forecast-get-response.interface';
import {ICurrentForecastGetResponse} from '../../contracts/current-forecast-get-response.interface';
import {IOWRequestSetting} from '../../core/ow-request-setting.interface';

export interface IDays {

    serviceUrl: string;

    getWeatherData(setting: IOWRequestSetting):
        Observable<IFiveDaysForecastGetResponse> | Observable<ISixteenDaysForecastGetResponse> | Observable<ICurrentForecastGetResponse>;
}
