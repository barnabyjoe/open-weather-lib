import {TestBed, inject} from '@angular/core/testing';

import {OwClientLibService} from './ow-client-lib.service';
import {FiveDayService} from './services/fivedays/fivedays.service';
import {SixteenDayService} from './services/sixteendays/sixteendays.service';
import {CurrentService} from './services/current/current.service';
import {ApiService} from './services/api.service';
import {UnitType} from './contracts/units.enum';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('OwClientLibService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [OwClientLibService,
        ApiService,
        FiveDayService,
        SixteenDayService,
        CurrentService]
    });
  });

  it('should be created', inject([OwClientLibService], (service: OwClientLibService) => {
    expect(service).toBeTruthy();
  }));

  it('should call GetFiveDaysForecastByCityName',
    inject([OwClientLibService, FiveDayService], (service: OwClientLibService, _fiveDayService: FiveDayService) => {
      // arrange
      _fiveDayService.getWeatherData = jasmine.createSpy().and.stub();

      service.GetFiveDaysForecastByCityName('London',
        'GB', UnitType.metric);

      // assert
      expect(_fiveDayService.getWeatherData).toHaveBeenCalled();
    }));

  it('should call GetSixteenDaysForecastByCityName', inject([OwClientLibService, SixteenDayService],
    (service: OwClientLibService, _sixteenDayService: SixteenDayService) => {
      // arrange
      _sixteenDayService.getWeatherData = jasmine.createSpy().and.stub();

      service.GetSixteenDaysForecastByCityName('London',
        'GB', UnitType.metric);

      // assert
      expect(_sixteenDayService.getWeatherData).toHaveBeenCalled();
    }));

  it('should call GetTodayForecastByCityName', inject([OwClientLibService, CurrentService],
    (service: OwClientLibService, _currentService: CurrentService) => {
      // arrange
      _currentService.getWeatherData = jasmine.createSpy().and.stub();

      service.GetTodayForecastByCityName('London',
        'GB', UnitType.metric);

      // assert
      expect(_currentService.getWeatherData).toHaveBeenCalled();
    }));

  it('should call GetTodayForecastByZipCode', inject([OwClientLibService, CurrentService],
    (service: OwClientLibService, _currentService: CurrentService) => {
      // arrange
      _currentService.getWeatherData = jasmine.createSpy().and.stub();

      service.GetTodayForecastByZipCode('49000',
        'UA', UnitType.metric);

      // assert
      expect(_currentService.getWeatherData).toHaveBeenCalled();
    }));

  it('should call GetSixteenDaysForecastByZipCode', inject([OwClientLibService, SixteenDayService],
    (service: OwClientLibService, _sixteenDayService: SixteenDayService) => {
      // arrange
      _sixteenDayService.getWeatherData = jasmine.createSpy().and.stub();

      service.GetSixteenDaysForecastByZipCode('49000',
        'UA', UnitType.metric);

      // assert
      expect(_sixteenDayService.getWeatherData).toHaveBeenCalled();
    }));

  it('should call GetFiveDaysForecastByZipCode', inject([OwClientLibService, FiveDayService],
    (service: OwClientLibService, _fiveDayService: FiveDayService) => {
      // arrange
      _fiveDayService.getWeatherData = jasmine.createSpy().and.stub();

      service.GetFiveDaysForecastByZipCode('49000',
        'UA', UnitType.metric);

      // assert
      expect(_fiveDayService.getWeatherData).toHaveBeenCalled();
    }));
});
