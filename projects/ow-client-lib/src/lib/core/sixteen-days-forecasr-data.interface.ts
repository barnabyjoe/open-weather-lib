import { IWeather } from './weather.interface';
import { ITemp } from './temp.interface';

export interface ISixteenDaysForecastData {
    dt: number;
    temp: ITemp;
    pressure: number;
    humidity: number;
    weather: IWeather;
    speed: number;
    deg: number;
    clouds: number;
    snow: number;
}
