import {OWRequestType} from '../contracts/ow-request-type.enum';
import {UnitType} from '../contracts/units.enum';

export interface IOWRequestSetting {
    queryName: string;
    countryCode: string;
    type: OWRequestType;
    units: UnitType;
}




