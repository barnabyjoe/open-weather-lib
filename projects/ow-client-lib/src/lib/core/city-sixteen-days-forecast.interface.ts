export interface ICitySixTeenDaysForecast {
    geoname_id: number;
    name: string;
    lat: number;
    lon: number;
    country: string;
    iso2: string;
    type: string;
    population: number;
}
