import { ICoord } from './coord.interface';

export interface ICityFiveDaysForecast {
    name: string;
    coord: ICoord;
    country: string;
}
