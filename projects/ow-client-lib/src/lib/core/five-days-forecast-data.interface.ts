import { IMain } from './main.interface';
import { IWeather } from './weather.interface';
import { IClouds } from './clouds.interface';
import { IWind } from './wind.interface';
import { IRain } from './rain.interface';
import { ISnow } from './snow.interface';

export interface IFiveDaysForecastData {
    dt: number;
    main: IMain;
    weather: Array<IWeather>;
    clouds: IClouds;
    wind: IWind;
    rain: IRain;
    snow: ISnow;
    dt_text: number;
}
