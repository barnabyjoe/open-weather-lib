import {OWRequestType} from '../contracts/ow-request-type.enum';
import {IOWRequestSetting} from './ow-request-setting.interface';
import {UnitType} from '../contracts/units.enum';

export class OWRequest {
    queryName: string;
    countryCode: string;
    type: OWRequestType;
    units: UnitType;

    constructor(setting: IOWRequestSetting) {
        if (setting) {
            Object.assign(this, setting);
        }
    }
}
