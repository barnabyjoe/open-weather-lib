import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { OwClientLibService } from './ow-client-lib.service';
import { SixteenDayService } from './services/sixteendays/sixteendays.service';
import { FiveDayService } from './services/fivedays/fivedays.service';
import { CurrentService } from './services/current/current.service';
import { ServerHostInterceptor } from './interceptors/server-host.interceptor';
import { ApiService } from './services/api.service';
@NgModule({
  imports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiService,
    SixteenDayService,
    FiveDayService,
    CurrentService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerHostInterceptor,
      multi: true
    },
    OwClientLibService
  ],
  exports: []
})
export class OwClientLibModule { }
